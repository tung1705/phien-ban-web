<!DOCTYPE html>
<html lang="en">
  @include('layouts.header')
  @include('layouts.sidebar')
	<div class="span9">
  <?php
   $giohang = session('giohang');
   $ds_sp = session('sanpham');
   $sum = 0;
  ?>
	<h3>  Giỏ hàng của bạn [ <small><?php echo "Có " . count($giohang) . " sản phẩm"; ?></small>]<a href="{{url('')}}"><button class="btn btn-large pull-right"> Tiếp tục mua sắm </button></a></h3>
	<hr class="soft">
	<table class="table table-bordered">
  <thead>
    <tr>
      <th>Sách</th>
      <th>Tác giả</th>
      <th>Số lượng</th>
		  <th>Giá một sản phẩm</th>
      <th>Thành tiền</th>
	  </tr>
  </thead>
  <tbody>
    @if(count($giohang) != 0)
    @foreach($ds_sp as $index=>$sp)
    <tr>
      <td> <img width="60" src="{{ asset('img/' . $sp->linkanh) }}" alt=""></td>
      <td><?php echo $sp->tacgia; ?></td>
		  <td><?php echo $giohang[$index]->soluong; ?></td>
      <td><?php echo $sp->giaban . " VNĐ"; ?></td>
      <td><?php
          $gia = $sp->giaban * $giohang[$index]->soluong;
          echo $gia . " VNĐ";
          $sum += $gia;
          ?></td>
    </tr>
    @endforeach
    @endif
		<tr>
      <td colspan="4" style="text-align:right"><strong>Tổng cộng</strong></td>
      <td class="label label-important" style="display:block"> <strong><?php if(count($giohang) != 0) echo $sum . " VNĐ";
                                                                              else echo "0 VNĐ"; ?></strong></td>
    </tr>
	</tbody>
  </table>
  <button class="btn btn-large pull-right" id="thanhtoan">Thanh toán</button>
</div>
</div></div>
</div>
@include('layouts.footer')
</body>
</html>
