<!DOCTYPE html>
<html lang="en">
  @include('layouts.header')
  @include('layouts.sidebar')

	<div class="span9">
	<div class="row">
			<div id="gallery" class="span3">
				<img src="{{asset('img/' . $sp->linkanh)}}" style="width:100%" alt="img">

			 <div class="btn-toolbar">
			  <div class="btn-group">
				<span class="btn"><i class="icon-envelope"></i></span>
				<span class="btn"><i class="icon-print"></i></span>
				<span class="btn"><i class="icon-zoom-in"></i></span>
				<span class="btn"><i class="icon-star"></i></span>
				<span class="btn"><i class="icon-thumbs-up"></i></span>
				<span class="btn"><i class="icon-thumbs-down"></i></span>
			  </div>
			</div>
			</div>
			<div class="span6">
				<h3><?php echo $sp->ten; ?> </h3>
				<small><?php echo "Tác giả: " . $sp->tacgia?></small>
        <small class="pull-right"><?php echo "Còn " . $sp->conlai . " sản phẩm"; ?></small>
				<hr class="soft">
				<form class="form-horizontal qtyFrm">
				  <div class="control-group">
					<label class="control-label"><span><?php echo $sp->giaban . " VNĐ"; ?></span></label>
					<div class="controls">
					<input name="soluong" type="number" class="span1" placeholder="Slượng">
					  <button class="btn btn-large btn-primary pull-right" data-spid="<?php echo $sp->id; ?>" > Thêm vào giỏ </button>
					</div>
				  </div>
				</form>

				<hr class="soft">
				<h4><?php echo "Còn " . $sp->conlai . " sản phẩm" ?></h4>

				<hr class="soft clr">

			</div>

			<div class="span9">
            <ul id="productDetail" class="nav nav-tabs">
              <li class="active"><a href="#home" data-toggle="tab">Chi tiết cuốn sách</a></li>
              <li><a href="#profile" data-toggle="tab">Sách cùng thể loại</a></li>
            </ul>
            <div id="myTabContent" class="tab-content">
              <div class="tab-pane fade active in" id="home">
			  <h4>Thông tin chi tiết</h4>
                <table class="table table-bordered">
				<tbody>
				<tr class="techSpecRow"><td class="techSpecTD1">Công ty phát hành</td><td class="techSpecTD2"><?php echo $sp->congtyphathanh; ?></td></tr>
				<tr class="techSpecRow"><td class="techSpecTD1">Nhà xuất bản</td><td class="techSpecTD2"><?php echo $sp->nxb; ?></td></tr>
        @if($sp->tacgia != null)
				<tr class="techSpecRow"><td class="techSpecTD1">Tác giả</td><td class="techSpecTD2"><?php echo $sp->tacgia; ?></td></tr>
        @endif
        @if($sp->dichgia != null)
        <tr class="techSpecRow"><td class="techSpecTD1">Dịch giả</td><td class="techSpecTD2"><?php echo $sp->dichgia; ?></td></tr>
        @endif
				<tr class="techSpecRow"><td class="techSpecTD1">Loại bìa</td><td class="techSpecTD2"><?php echo $sp->loaibia; ?></td></tr>
				<tr class="techSpecRow"><td class="techSpecTD1">Số trang</td><td class="techSpecTD2"><?php echo $sp->sotrang; ?></td></tr>
        <tr class="techSpecRow"><td class="techSpecTD1">Ngày xuất bản</td><td class="techSpecTD2"><?php echo $sp->ngayxuatban; ?></td></tr>
        <tr class="techSpecRow"><td class="techSpecTD1">SKU</td><td class="techSpecTD2"><?php echo $sp->sku; ?></td></tr>
				</tbody>
				</table>

				<h5>Giới thiệu sách</h5>
				<pre>
				<?php echo $sp->noidungchinh; ?>
      </pre>

              </div>
		<div class="tab-pane fade" id="profile">
		<div class="tab-content">

			<div class="tab-pane active" id="blockView">
				<ul class="thumbnails">
          @foreach($ds_sp as $index=>$sp)
				  <li class="span3">
				   <div class="thumbnail">
           <img src="{{ asset('img/' . $sp->linkanh) }}" />
					 <div class="caption">
					 <h5><?php if(strlen($sp->ten) > 24) echo substr($sp->ten, 0, 24) . '...';
                     else echo $sp->ten; ?></h5>
           <h5 style="text-align:center"><?php echo $sp->gia . " VNĐ" ?></h5>
           <h4 style="text-align:center"><a href="{{ url('/productdetail/' . $sp->id) }}" class="btn btn-secondary">Xem</a><a href="{{ url('/productdetail/' . $sp->id) }}" class="btn btn-primary">Thêm vào giỏ</a></h4>
					 </div>
				   </div>
				  </li>
          @endforeach
          {{ $ds_sp->links('pagination.pagination_links') }}
				  </ul>
		</div>
  </div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
@include('layouts.footer')
</body>
</html>
