<!DOCTYPE html>
<html lang="en">
  @include('layouts.header')
  @include('layouts.sidebar')
		<div class="span9">
			<div class="well well-small">
			<h4>Sản phẩm mới nhất</h4>
			<div class="row-fluid">
			<div id="featured" class="carousel slide">
			<div class="carousel-inner">
        <?php
        $pointer = 0;
        for($i = 1; $i <= 4; $i++)
        {
         ?>
         @if($i == 1)
         <div class="item active">
         @else
         <div class="item">
         @endif
         <ul class="thumbnails">
         <?php
         for($j = $pointer; $j < $pointer+4; $j++)
         {
           $sp = $danhsach_sp['lastest'][$j];
          ?>
          <li class="span3">
            <div class="thumbnail">
            <i class="tag"></i>
            <a href="#"><img src="{{ asset('img/' . $sp->linkanh) }}" alt="img"></a>
            <div class="caption">
              <h5><?php if(strlen($sp->ten) > 24) echo substr($sp->ten, 0, 24) . '...';
                        else echo $sp->ten; ?></h5>
              <h4><a href="{{ url('/productdetail/' . $sp->id) }}" class="btn btn-secondary">Xem</a></button><span class="pull-right"><?php echo $sp->giaban . " VNĐ"; ?></span></h4>
  					</div>
  				  </div>
          </li>
          <?php
          }
           ?>
         </ul>
         </div>
         <?php
         $pointer = $pointer+4;
         }
          ?>
			  </div>
			  <a class="left carousel-control" href="#featured" data-slide="prev">‹</a>
			  <a class="right carousel-control" href="#featured" data-slide="next">›</a>
			  </div>
			  </div>
		</div>
		<h4>Sản phẩm bán chạy nhất</h4>
			  <ul class="thumbnails">
          @foreach($danhsach_sp['most_sell'] as $index=>$sp)
				  <li class="span3">
				   <div class="thumbnail">
           <img src="{{ asset('img/' . $sp->linkanh) }}" />
					 <div class="caption">
					 <h5><?php if(strlen($sp->ten) > 35) echo substr($sp->ten, 0, 35) . '...';
                     else echo $sp->ten; ?></h5>
           <h5 style="text-align:center"><?php echo $sp->giaban . " VNĐ" ?></h5>
					 <h4 style="text-align:center"><a href="{{ url('/productdetail/' . $sp->id) }}" class="btn btn-secondary">Xem</a><a href="{{ url('/productdetail/' . $sp->id) }}" class="btn btn-primary">Thêm vào giỏ</a></h4>
					 </div>
				   </div>
				  </li>
          @endforeach
          {{ $danhsach_sp['most_sell']->links('pagination.pagination_links') }}
			  </ul>
		</div>
		</div>
	</div>
</div>
@include('layouts.footer')
</body>
</html>
