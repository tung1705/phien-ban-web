<head>
  <meta charset="utf-8">
  <title>Bookstore online</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">
  <meta name="csrf-token" content="{{ csrf_token() }}">
<!--Less styles -->
 <!-- Other Less css file //different less files has different color scheam
<link rel="stylesheet/less" type="text/css" href="themes/less/simplex.less">
<link rel="stylesheet/less" type="text/css" href="themes/less/classified.less">
<link rel="stylesheet/less" type="text/css" href="themes/less/amelia.less">  MOVE DOWN TO activate
-->
<!--<link rel="stylesheet/less" type="text/css" href="themes/less/bootshop.less">
<script src="themes/js/less.js" type="text/javascript"></script> -->


<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script>
$.ajaxSetup({
  headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  })
  </script>
  <script src="/js/thanhtoan.js"></script>
  <script src="/js/themvaogio.js"></script>
	<script src="{{asset('js/bootstrap.min.js')}}" type="text/javascript"></script>

<!-- Bootstrap style -->
  <link id="callCss" rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}" media="screen"/>
  <link href="{{asset('css/base.css')}}" rel="stylesheet" media="screen"/>
<!-- Bootstrap style responsive -->
<link href="{{asset('css/bootstrap-responsive.min.css')}}" rel="stylesheet"/>
<link href="{{asset('css/font-awesome.css')}}" type="text/css" rel="stylesheet">
<!-- Google-code-prettify -->
<link href="{{asset('css/prettify.css')}}" rel="stylesheet"/>
<!-- fav and touch icons -->
  <link rel="shortcut icon" href="icon/favicon.ico">
  <link rel="apple-touch-icon-precomposed" sizes="144x144" href="{{asset('img/apple-touch-icon-144-precomposed.png')}}">
  <link rel="apple-touch-icon-precomposed" sizes="114x114" href="{{asset('img/apple-touch-icon-114-precomposed.png')}}">
  <link rel="apple-touch-icon-precomposed" sizes="72x72" href="{{asset('img/apple-touch-icon-72-precomposed.png')}}">
  <link rel="apple-touch-icon-precomposed" href="{{asset('img/apple-touch-icon-57-precomposed.png')}}">
<style type="text/css" id="enject"></style>

</head>
<body>
<div id="header">
<div class="container">
<div id="welcomeLine" class="row">
<div class="span6">Xin chào <strong>{{session('username')}}</strong></div>
<div class="span6">
<div class="pull-right">
  @if(session('userid') != null)
  <a href="{{url('/logout')}}"><span class="btn btn-large btn-success">Đăng xuất</span></a>
  @else
  <a href="#login" role="button" data-toggle="modal" style="padding-right:0"><span class="btn btn-large btn-success">Đăng nhập</span></a>
  @endif
 <div id="login" class="modal hide fade in" tabindex="-1" role="dialog" aria-labelledby="login" aria-hidden="false" >
     <div class="modal-header">
     <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
     <h3>Đăng nhập</h3>
     </div>
     <div class="modal-body">
     <form class="form-horizontal loginFrm" method="post" action="{{url('')}}">
       {!! csrf_field() !!}
       <div class="control-group">
       <input type="text" name="username" placeholder="tài khoản">
       </div>
       <div class="control-group">
       <input type="password" name="password" placeholder="mật khẩu">
       </div>
       <div class="control-group">
       <label class="checkbox">
       </label>
       </div>
       <button type="submit" class="btn btn-success">Đăng nhập</button>
     </form>
     <a class="btn btn-success">Đăng nhập bằng google</a>
     </div>
 </div>
</div>
</div>
</div>
<!-- Navbar ================================================== -->
<div id="logoArea" class="navbar">
<a id="smallScreen" data-target="#topMenu" data-toggle="collapse" class="btn btn-navbar">
<span class="icon-bar"></span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
</a>
<div class="navbar-inner">
  <a class="brand" href="{{route('home')}}"><img src="{{asset('img/logo.png')}}" alt="Bootsshop"/></a>
  <form class="form-inline navbar-search" method="post" action="{{url('/product/search')}}" >
    {!! csrf_field() !!}
    <input name="infor" id="srchFld" class="srchTxt" type="text" />
    <select class="srchTxt" name="theloai">
      <option value="tatca">Tất cả</option>
      <option value="vanhoc">Sách văn học</option>
      <option value="kinhte">Sách kinh tế</option>
      <option value="giaotrinh">Sách giáo trình</option>
      <option value="lichsu">Sách lịch sử</option>
      <option value="khkt">Sách khoa học - kĩ thuật</option>
    </select>
    <button type="submit" id="submitButton" class="btn btn-primary">Tìm kiếm</button>
  </form>
  <ul id="topMenu" class="nav pull-right">
 <li class=""><a >Tư vấn</a></li>
 <li class=""><a >Liên hệ</a></li>
  </ul>
</div>
</div>
</div>
</div>
<!-- Header End====================================================================== -->

  </div>
</div>
<div id="mainBody">
<div class="container">
<div class="row">
