<html lang="en">
@include('layouts.header')
@include('layouts.sidebar')
	<div class="span9">

	<h3>
    @if($theloai == 'vanhoc')
    <?php echo "Sách văn học"; ?>
    @elseif($theloai == 'kinhte')
    <?php echo "Sách kinh tế"; ?>
    @elseif($theloai == 'giaotrinh')
    <?php echo "Sách giáo trình"; ?>
    @elseif($theloai == 'lichsu')
    <?php echo "Sách lịch sử"; ?>
    @elseif($theloai == 'khkt')
    <?php echo "Sách khoa học - kĩ thuật"; ?>
    @endif
    <small class="pull-right"><?php echo "Có " . count($ds_sp) . " sản phẩm"; ?></small></h3>
	<hr class="soft">

  <form class="form-horizontal span6">
    <div class="control-group">
      <label class="control-label alignL">Sắp xếp theo</label>
      <select onchange="window.location.href=this.value;">
        <option value="{{url('/product/' . $theloai)}}"></option>
        <option value="{{url('/product/' . $theloai . '/az')}}">Tên sách A - Z</option>
        <option value="{{url('/product/' . $theloai . '/za')}}">Tên sách Z - A</option>
        <option value="{{url('/product/' . $theloai . '/lowhigh')}}">Giá thấp - cao</option>
        <option value="{{url('/product/' . $theloai . '/highlow')}}">Giá cao - thấp</option>
      </select>
    </div>
    </form>

  @include('sanpham')
