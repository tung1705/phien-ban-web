<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Support\Facades\DB;

class SanPhamModel extends Model
{
    protected $table = 'SanPham';

    public static function getAllSp()
    {
      $sp = SanPhamModel::get();
      return $sp;
    }

    public static function getLastestProduct()
    {
      // $sp = SanPhamModel::lastest()->limit(5)->get();
      $sp = DB::table('SanPham')
                ->latest()
                ->limit(16)
                ->get();
      return $sp;
    }

    public static function getRandom()
    {
      // $sp = SanPhamModel::inRandomOrder()->get();
      $sp = DB::table('SanPham')
                ->inRandomOrder()
                ->get();
      return $sp;
    }

    public static function getModeProduct()
    {
      $sp = DB::table('SanPham')
                ->orderBy('likes', 'desc')
                ->limit(15)
                ->get();
      return $sp;
    }

    public static function getMostSellProduct()
    {
      $sp = DB::table('SanPham')
                ->orderBy('daban', 'desc')
                ->paginate(6);
      return $sp;
    }

    public static function getProduct($spId)
    {
      $sp = SanPhamModel::find($spId);
      return $sp;
    }

    public static function getSachTheoTheLoai($theloai)
    {
      $ds_sp = SanPhamModel::where('loai', '=', $theloai)
                          ->paginate(6);
      return $ds_sp;
    }

    public static function getSachTheoTheLoaiAZ($theloai)
    {
      $ds_sp = SanPhamModel::where('loai', '=', $theloai)
                          ->orderBy('ten', 'asc')
                          ->paginate(6);
      return $ds_sp;
    }

    public static function getSachTheoTheLoaiZA($theloai)
    {
      $ds_sp = SanPhamModel::where('loai', '=', $theloai)
                          ->orderBy('ten', 'desc')
                          ->paginate(6);
      return $ds_sp;
    }

    public static function getSachTheoTheLoaiLowHigh($theloai)
    {
      $ds_sp = SanPhamModel::where('loai', '=', $theloai)
                          ->orderBy('gia', 'asc')
                          ->paginate(6);
      return $ds_sp;
    }

    public static function getSachTheoTheLoaiHighLow($theloai)
    {
      $ds_sp = SanPhamModel::where('loai', '=', $theloai)
                          ->orderBy('gia', 'desc')
                          ->paginate(6);
      return $ds_sp;
    }

    public static function getSach($infor)
    {
      $ds_sp = SanPhamModel::where("ten", "like", "%$infor%")
                            ->orWhere("congtyphathanh", "like", "%$infor%")
                            ->orWhere("nxb", "like", "%$infor%")
                            ->orWhere("tacgia", "like", "%$infor%")
                            ->orWhere("dichgia", "like", "%$infor%")
                            ->orWhere("congtyphathanh", "like", "%$infor%")
                            ->orWhere("noidungchinh", "like", "%$infor%")
                            ->orWhere("gia", "=", $infor)
                            ->paginate(6);
      return $ds_sp;
    }

    public static function getSachVanHoc($infor)
    {
      $ds_sp = SanPhamModel::where([["loai", "=", "vanhoc"], ["ten", "like", "%$infor%"],])
                            ->orWhere([["loai", "=", "vanhoc"], ["congtyphathanh", "like", "%$infor%"],])
                            ->orWhere([["loai", "=", "vanhoc"], ["nxb", "like", "%$infor%"],])
                            ->orWhere([["loai", "=", "vanhoc"], ["tacgia", "like", "%$infor%"],])
                            ->orWhere([["loai", "=", "vanhoc"], ["dichgia", "like", "%$infor%"],])
                            ->orWhere([["loai", "=", "vanhoc"], ["congtyphathanh", "like", "%$infor%"],])
                            ->orWhere([["loai", "=", "vanhoc"], ["noidungchinh", "like", "%$infor%"],])
                            ->orWhere([["loai", "=", "vanhoc"], ["gia", "=", $infor],])
                            ->paginate(6);
      return $ds_sp;
    }

    public static function getSachKinhTe($infor)
    {
      $ds_sp = SanPhamModel::where([["loai", "=", "kinhte"], ["ten", "like", "%$infor%"],])
                            ->orWhere([["loai", "=", "kinhte"], ["congtyphathanh", "like", "%$infor%"],])
                            ->orWhere([["loai", "=", "kinhte"], ["nxb", "like", "%$infor%"],])
                            ->orWhere([["loai", "=", "kinhte"], ["tacgia", "like", "%$infor%"],])
                            ->orWhere([["loai", "=", "kinhte"], ["dichgia", "like", "%$infor%"],])
                            ->orWhere([["loai", "=", "kinhte"], ["congtyphathanh", "like", "%$infor%"],])
                            ->orWhere([["loai", "=", "kinhte"], ["noidungchinh", "like", "%$infor%"],])
                            ->orWhere([["loai", "=", "kinhte"], ["gia", "=", $infor],])
                            ->paginate(6);
      return $ds_sp;
    }

    public static function getSachGiaoTrinh($infor)
    {
      $ds_sp = SanPhamModel::where([["loai", "=", "giaotrinh"], ["ten", "like", "%$infor%"],])
                            ->orWhere([["loai", "=", "giaotrinh"], ["congtyphathanh", "like", "%$infor%"],])
                            ->orWhere([["loai", "=", "giaotrinh"], ["nxb", "like", "%$infor%"],])
                            ->orWhere([["loai", "=", "giaotrinh"], ["tacgia", "like", "%$infor%"],])
                            ->orWhere([["loai", "=", "giaotrinh"], ["dichgia", "like", "%$infor%"],])
                            ->orWhere([["loai", "=", "giaotrinh"], ["congtyphathanh", "like", "%$infor%"],])
                            ->orWhere([["loai", "=", "giaotrinh"], ["noidungchinh", "like", "%$infor%"],])
                            ->orWhere([["loai", "=", "giaotrinh"], ["gia", "=", $infor],])
                            ->paginate(6);
      return $ds_sp;
    }

    public static function getSachLichSu($infor)
    {
      $ds_sp = SanPhamModel::where([["loai", "=", "lichsu"], ["ten", "like", "%$infor%"],])
                            ->orWhere([["loai", "=", "lichsu"], ["congtyphathanh", "like", "%$infor%"],])
                            ->orWhere([["loai", "=", "lichsu"], ["nxb", "like", "%$infor%"],])
                            ->orWhere([["loai", "=", "lichsu"], ["tacgia", "like", "%$infor%"],])
                            ->orWhere([["loai", "=", "lichsu"], ["dichgia", "like", "%$infor%"],])
                            ->orWhere([["loai", "=", "lichsu"], ["congtyphathanh", "like", "%$infor%"],])
                            ->orWhere([["loai", "=", "lichsu"], ["noidungchinh", "like", "%$infor%"],])
                            ->orWhere([["loai", "=", "lichsu"], ["gia", "=", $infor],])
                            ->paginate(6);
      return $ds_sp;
    }

    public static function getSachKHKT($infor)
    {
      $ds_sp = SanPhamModel::where([["loai", "=", "khkt"], ["ten", "like", "%$infor%"],])
                            ->orWhere([["loai", "=", "khkt"], ["congtyphathanh", "like", "%$infor%"],])
                            ->orWhere([["loai", "=", "khkt"], ["nxb", "like", "%$infor%"],])
                            ->orWhere([["loai", "=", "khkt"], ["tacgia", "like", "%$infor%"],])
                            ->orWhere([["loai", "=", "khkt"], ["dichgia", "like", "%$infor%"],])
                            ->orWhere([["loai", "=", "khkt"], ["congtyphathanh", "like", "%$infor%"],])
                            ->orWhere([["loai", "=", "khkt"], ["noidungchinh", "like", "%$infor%"],])
                            ->orWhere([["loai", "=", "khkt"], ["gia", "=", $infor],])
                            ->paginate(6);
      return $ds_sp;
    }
}
