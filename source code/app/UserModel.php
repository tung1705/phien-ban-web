<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserModel extends Model
{
  protected $table = 'users';

  public static function hasAccount($username, $password)
  {
    $user = UserModel::where([
      ['username', $username],
      ['password', $password],
      ])->first();
      if(!empty($user)) return $user;
      else return 0;
    }
  }
