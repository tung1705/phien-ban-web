<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DongHoaDonModel extends Model
{
    protected $table = 'donghoadon';

    public static function create($spid, $giamua, $giaban, $soluong)
    {
      $new_donghoadon = new DongHoaDonModel();
      $new_donghoadon->sp_id = $spid;
      $new_donghoadon->giamua = $giamua;
      $new_donghoadon->giaban = $giaban;
      $new_donghoadon->soluong = $soluong;
      return $new_donghoadon;
    }
}
