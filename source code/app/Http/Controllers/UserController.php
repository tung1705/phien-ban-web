<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\UserModel;

use App\SanPhamModel;

use Session;

use Illuminate\Support\Facades\DB;

class UserController extends Controller
{
     public function checkLogin(Request $request)
     {
       $user_name = $request->input('username');
       $password = $request->input('password');
       $user = UserModel::hasAccount($user_name, $password);
       if($user)
       {

         session(['userid' => $user->id]);
         session(['username' => $user->ten]);
         //session(['giohang' => [] ]);
         //session(['sanpham' => [] ]);
         $most_sell_sp = SanPhamModel::getMostSellProduct();
         $lastest_sp = SanPhamModel::getLastestProduct();
         return view('index', array('danhsach_sp'=> ['most_sell'=>$most_sell_sp, 'lastest'=>$lastest_sp]));
       }
     }
}
