<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\SanPhamModel;

use App\DongHoaDonModel;

use App\HoaDonModel;

use Session;

use Illuminate\Support\Facades\DB;

class MuaHangController extends Controller
{
  public function addToCart(Request $request)
  {
    $spid = $request->input('spid');
    $soluong = $request->input('soluong');
    $sp = SanPhamModel::getProduct($spid);
    $giamua = $sp->giamua;
    $giaban = $sp->giaban;
    $new_donghoadon = DongHoaDonModel::create($spid, $giamua, $giaban, $soluong);

    if(session('giohang') == null)
    {
      session()->put('giohang', [$new_donghoadon, ]);
      session()->put('sanpham', [$sp, ]);
      session()->save();
    }
    else
    {
      $giohang = session('giohang');
      $ds_sp = session('sanpham');
      $giohang[] = $new_donghoadon;
      $ds_sp[] = $sp;
      //session(['giohang' => $giohang]);
      //session(['sanpham' => $ds_sp]);
      session()->push('giohang', $new_donghoadon);
      session()->push('sanpham', $sp);
      session()->save();
    }
  }

  public function showCart()
  {
    return view('giohang');
  }

  public function thanhToan(Request $request)
  {

  }
}
