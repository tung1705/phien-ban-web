<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\SanPhamModel;

use Illuminate\Support\Facades\DB;

class SanPhamController extends Controller
{
    public function show()
    {
      $most_sell_sp = SanPhamModel::getMostSellProduct();
      $lastest_sp = SanPhamModel::getLastestProduct();
      return view('index', array('danhsach_sp'=> ['most_sell'=>$most_sell_sp, 'lastest'=>$lastest_sp]));
    }

    public function seeDetail($spid)
    {
      $sp = SanPhamModel::getProduct($spid);
      $theloai = $sp->loai;
      $ds_sp = SanPhamModel::getSachTheoTheLoai($theloai);
      return view('productdetail', ['sp' => $sp, 'ds_sp' => $ds_sp]);
    }

    public function showProduct($theloai)
    {
      $ds_sp = SanPhamModel::getSachTheoTheLoai($theloai);
      return view('sanphamtheotheloai', ['theloai' => $theloai, 'ds_sp' => $ds_sp]);
    }

    public function showProductAZ($theloai)
    {
      $ds_sp = SanPhamModel::getSachTheoTheLoaiAZ($theloai);
      return view('sanphamtheotheloai', ['theloai' => $theloai, 'ds_sp' => $ds_sp]);
    }

    public function showProductZA($theloai)
    {
      $ds_sp = SanPhamModel::getSachTheoTheLoaiZA($theloai);
      return view('sanphamtheotheloai', ['theloai' => $theloai, 'ds_sp' => $ds_sp]);
    }

    public function showProductLowHigh($theloai)
    {
      $ds_sp = SanPhamModel::getSachTheoTheLoaiLowHigh($theloai);
      return view('sanphamtheotheloai', ['theloai' => $theloai, 'ds_sp' => $ds_sp]);
    }

    public function showProductHighLow($theloai)
    {
      $ds_sp = SanPhamModel::getSachTheoTheLoaiHighLow($theloai);
      return view('sanphamtheotheloai', ['theloai' => $theloai, 'ds_sp' => $ds_sp]);
    }

    public function search(Request $request)
    {
      $theloai = $request->input('theloai');
      $infor = $request->input('infor');

      if($theloai == "tatca") $ds_sp = SanPhamModel::getSach($infor);
      elseif($theloai == "vanhoc") $ds_sp = SanPhamModel::getSachVanHoc($infor);
      elseif($theloai == "kinhte") $ds_sp = SanPhamModel::getSachKinhTe($infor);
      elseif($theloai == "giaotrinh") $ds_sp = SanPhamModel::getSachGiaoTrinh($infor);
      elseif($theloai == "lichsu") $ds_sp = SanPhamModel::getSachLichSu($infor);
      else $ds_sp = SanPhamModel::getSachKHKT($infor);

      return view('sanphamtimkiem', ['ds_sp' => $ds_sp]);
    }

}
