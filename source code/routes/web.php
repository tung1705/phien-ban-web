<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('', 'SanPhamController@show')->name('home');

Route::get('productdetail/{spid}','SanPhamController@seeDetail');

Route::get('product/{theloai}', 'SanPhamController@showProduct');

Route::get('product/{theloai}/az', 'SanPhamController@showProductAZ');

Route::get('product/{theloai}/za', 'SanPhamController@showProductZA');

Route::get('product/{theloai}/lowhigh', 'SanPhamController@showProductLowHigh');

Route::get('product/{theloai}/highlow', 'SanPhamController@showProductHighLow');

Route::post('product/search', 'SanPhamController@search');

Route::post('', 'UserController@checkLogin');

Route::get('logout', ['as'=>'logout', function(){
    session()->flush();
    return redirect()->route('home');
}]);

Route::post('product/addtocart', 'MuaHangController@addToCart')->middleware('CheckLogin');

Route::get('yourcart', 'MuaHangController@showCart')->middleware('CheckLogin');

Route::get('thanhtoan', 'MuaHangController@thanhToan');
