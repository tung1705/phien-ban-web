$(function() {
    $(".btn.btn-large.btn-primary.pull-right").click(function(){
      var spid = $(this).data('spid');
      var soluong = document.getElementsByName("soluong")[0].value;
      if(soluong == null || soluong == "")
      {
        alert("Bạn chưa chọn số lượng sản phẩm!");
      }
      else
      {
      $.ajax({
        url: "/product/addtocart",
        type:'POST',
        data:{
          spid:spid,
          soluong:soluong
        },
        success: function(html){
          //alert("đã thêm vào giỏ");
          window.history.back();
        }
      });
      }
    });
})
