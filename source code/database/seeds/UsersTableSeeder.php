<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
          'username' => 'tung1705',
          'password' => '1',
          'email' => 'ductungnguyen1997@gmail.com',
          'phone' => '0984431825',
          'status' => 1,
          'roleid' => 1
        ]);
    }
}
