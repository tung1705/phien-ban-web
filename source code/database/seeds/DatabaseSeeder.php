<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('SanPham')->insert([
        'loai' => 'vanhoc',
        'ten' => 'Thanh xuân không hối tiếc',
        'gia' => 80000,
        'linkanh' => 'thanhxuankhonghoitiec.jpg',
        'congtyphathanh' => 'Trí Việt',
        'nxb' => 'Nhà xuất bản Văn học',
        'tacgia' => 'Jack London',
        'dichgia' => 'Nhiều người dịch',
        'loaibia' => 'Bìa mềm',
        'sotrang' => 312,
        'ngayxuatban' => '10-2017',
        'sku' => '2519727431083',
        'noidungchinh' => 'Thanh Xuân Không Hối Tiếc

Mỗi người có một cách khác nhau để sống những ngày tuổi trẻ, có người dành trọn nó cho những cuộc tình, có người dành trọn nó cho công việc, có người dành trọn nó để tự yêu thương mình, và cũng có những người chia tuổi trẻ của mình ra, để yêu một vài người, sau đó yêu mình, yêu người xung quanh mình, rồi đến một lúc nào đó thích hợp mới tiếp tục muốn yêu thêm một người cho đến hết cuộc đời.

Dù người ta có dành tuổi trẻ của mình cho ai hay để làm gì, thì cũng mong sau này khi đã đủ chín chắn để ngoái đầu nhìn lại, họ cũng sẽ mỉm cười, một nụ cười vô ưu viên mãn.'
      ]);
      DB::table('SanPham')->insert([
        'loai' => 'vanhoc',
        'ten' => 'Nam và Sylvie',
        'gia' => 75000,
        'linkanh' => 'namvasylvie.jpg',
        'congtyphathanh' => 'Trí Việt',
        'nxb' => 'Nhà xuất bản Văn học',
        'tacgia' => 'Jack London',
        'dichgia' => 'Nhiều người dịch',
        'loaibia' => 'Bìa mềm',
        'sotrang' => 312,
        'ngayxuatban' => '10-2017',
        'sku' => '2519727431083',
        'noidungchinh' => 'Thanh Xuân Không Hối Tiếc

Mỗi người có một cách khác nhau để sống những ngày tuổi trẻ, có người dành trọn nó cho những cuộc tình, có người dành trọn nó cho công việc, có người dành trọn nó để tự yêu thương mình, và cũng có những người chia tuổi trẻ của mình ra, để yêu một vài người, sau đó yêu mình, yêu người xung quanh mình, rồi đến một lúc nào đó thích hợp mới tiếp tục muốn yêu thêm một người cho đến hết cuộc đời.

Dù người ta có dành tuổi trẻ của mình cho ai hay để làm gì, thì cũng mong sau này khi đã đủ chín chắn để ngoái đầu nhìn lại, họ cũng sẽ mỉm cười, một nụ cười vô ưu viên mãn.'
      ]);
      DB::table('SanPham')->insert([
        'loai' => 'vanhoc',
        'ten' => 'Bước chân theo dấu mặt trời',
        'gia' => 119000,
        'linkanh' => 'buocchantheodaumattroi.jpg',
        'congtyphathanh' => 'Trí Việt',
        'nxb' => 'Nhà xuất bản Văn học',
        'tacgia' => 'Jack London',
        'dichgia' => 'Nhiều người dịch',
        'loaibia' => 'Bìa mềm',
        'sotrang' => 312,
        'ngayxuatban' => '10-2017',
        'sku' => '2519727431083',
        'noidungchinh' => 'Thanh Xuân Không Hối Tiếc

Mỗi người có một cách khác nhau để sống những ngày tuổi trẻ, có người dành trọn nó cho những cuộc tình, có người dành trọn nó cho công việc, có người dành trọn nó để tự yêu thương mình, và cũng có những người chia tuổi trẻ của mình ra, để yêu một vài người, sau đó yêu mình, yêu người xung quanh mình, rồi đến một lúc nào đó thích hợp mới tiếp tục muốn yêu thêm một người cho đến hết cuộc đời.

Dù người ta có dành tuổi trẻ của mình cho ai hay để làm gì, thì cũng mong sau này khi đã đủ chín chắn để ngoái đầu nhìn lại, họ cũng sẽ mỉm cười, một nụ cười vô ưu viên mãn.'
      ]);
      DB::table('SanPham')->insert([
        'loai' => 'vanhoc',
        'ten' => 'Ngủ thật lâu - Yêu thật sâu',
        'gia' => 59000,
        'linkanh' => 'nguthatlauyeuthatsau.jpg',
        'congtyphathanh' => 'Trí Việt',
        'nxb' => 'Nhà xuất bản Văn học',
        'tacgia' => 'Jack London',
        'dichgia' => 'Nhiều người dịch',
        'loaibia' => 'Bìa mềm',
        'sotrang' => 312,
        'ngayxuatban' => '10-2017',
        'sku' => '2519727431083',
        'noidungchinh' => 'Thanh Xuân Không Hối Tiếc

Mỗi người có một cách khác nhau để sống những ngày tuổi trẻ, có người dành trọn nó cho những cuộc tình, có người dành trọn nó cho công việc, có người dành trọn nó để tự yêu thương mình, và cũng có những người chia tuổi trẻ của mình ra, để yêu một vài người, sau đó yêu mình, yêu người xung quanh mình, rồi đến một lúc nào đó thích hợp mới tiếp tục muốn yêu thêm một người cho đến hết cuộc đời.

Dù người ta có dành tuổi trẻ của mình cho ai hay để làm gì, thì cũng mong sau này khi đã đủ chín chắn để ngoái đầu nhìn lại, họ cũng sẽ mỉm cười, một nụ cười vô ưu viên mãn.'
      ]);
      DB::table('SanPham')->insert([
        'loai' => 'vanhoc',
        'ten' => 'Nhà giả kim',
        'gia' => 69000,
        'linkanh' => 'nhagiakim.jpg',
        'congtyphathanh' => 'Trí Việt',
        'nxb' => 'Nhà xuất bản Văn học',
        'tacgia' => 'Jack London',
        'dichgia' => 'Nhiều người dịch',
        'loaibia' => 'Bìa mềm',
        'sotrang' => 312,
        'ngayxuatban' => '10-2017',
        'sku' => '2519727431083',
        'noidungchinh' => 'Thanh Xuân Không Hối Tiếc

Mỗi người có một cách khác nhau để sống những ngày tuổi trẻ, có người dành trọn nó cho những cuộc tình, có người dành trọn nó cho công việc, có người dành trọn nó để tự yêu thương mình, và cũng có những người chia tuổi trẻ của mình ra, để yêu một vài người, sau đó yêu mình, yêu người xung quanh mình, rồi đến một lúc nào đó thích hợp mới tiếp tục muốn yêu thêm một người cho đến hết cuộc đời.

Dù người ta có dành tuổi trẻ của mình cho ai hay để làm gì, thì cũng mong sau này khi đã đủ chín chắn để ngoái đầu nhìn lại, họ cũng sẽ mỉm cười, một nụ cười vô ưu viên mãn.'
      ]);
      DB::table('SanPham')->insert([
        'loai' => 'vanhoc',
        'ten' => 'Khi hơi thở hóa thinh không',
        'gia' => 109000,
        'linkanh' => 'khihoithohoathinhkhong.jpg',
        'congtyphathanh' => 'Trí Việt',
        'nxb' => 'Nhà xuất bản Văn học',
        'tacgia' => 'Jack London',
        'dichgia' => 'Nhiều người dịch',
        'loaibia' => 'Bìa mềm',
        'sotrang' => 312,
        'ngayxuatban' => '10-2017',
        'sku' => '2519727431083',
        'noidungchinh' => 'Thanh Xuân Không Hối Tiếc

Mỗi người có một cách khác nhau để sống những ngày tuổi trẻ, có người dành trọn nó cho những cuộc tình, có người dành trọn nó cho công việc, có người dành trọn nó để tự yêu thương mình, và cũng có những người chia tuổi trẻ của mình ra, để yêu một vài người, sau đó yêu mình, yêu người xung quanh mình, rồi đến một lúc nào đó thích hợp mới tiếp tục muốn yêu thêm một người cho đến hết cuộc đời.

Dù người ta có dành tuổi trẻ của mình cho ai hay để làm gì, thì cũng mong sau này khi đã đủ chín chắn để ngoái đầu nhìn lại, họ cũng sẽ mỉm cười, một nụ cười vô ưu viên mãn.'
      ]);

      DB::table('SanPham')->insert([
        'loai' => 'kinhte',
        'ten' => 'Tiếp thị 4.0',
        'gia' => 69000,
        'linkanh' => 'tiepthi40.jpg',
        'congtyphathanh' => 'Trí Việt',
        'nxb' => 'Nhà xuất bản Văn học',
        'tacgia' => 'Jack London',
        'dichgia' => 'Nhiều người dịch',
        'loaibia' => 'Bìa mềm',
        'sotrang' => 312,
        'ngayxuatban' => '10-2017',
        'sku' => '2519727431083',
        'noidungchinh' => 'Thanh Xuân Không Hối Tiếc

Mỗi người có một cách khác nhau để sống những ngày tuổi trẻ, có người dành trọn nó cho những cuộc tình, có người dành trọn nó cho công việc, có người dành trọn nó để tự yêu thương mình, và cũng có những người chia tuổi trẻ của mình ra, để yêu một vài người, sau đó yêu mình, yêu người xung quanh mình, rồi đến một lúc nào đó thích hợp mới tiếp tục muốn yêu thêm một người cho đến hết cuộc đời.

Dù người ta có dành tuổi trẻ của mình cho ai hay để làm gì, thì cũng mong sau này khi đã đủ chín chắn để ngoái đầu nhìn lại, họ cũng sẽ mỉm cười, một nụ cười vô ưu viên mãn.'
      ]);
      DB::table('SanPham')->insert([
        'loai' => 'kinhte',
        'ten' => 'Để không bao giờ thất nghiệp',
        'gia' => 69000,
        'linkanh' => 'dekhongbaogiothatnghiep.jpg',
        'congtyphathanh' => 'Trí Việt',
        'nxb' => 'Nhà xuất bản Văn học',
        'tacgia' => 'Jack London',
        'dichgia' => 'Nhiều người dịch',
        'loaibia' => 'Bìa mềm',
        'sotrang' => 312,
        'ngayxuatban' => '10-2017',
        'sku' => '2519727431083',
        'noidungchinh' => 'Thanh Xuân Không Hối Tiếc

Mỗi người có một cách khác nhau để sống những ngày tuổi trẻ, có người dành trọn nó cho những cuộc tình, có người dành trọn nó cho công việc, có người dành trọn nó để tự yêu thương mình, và cũng có những người chia tuổi trẻ của mình ra, để yêu một vài người, sau đó yêu mình, yêu người xung quanh mình, rồi đến một lúc nào đó thích hợp mới tiếp tục muốn yêu thêm một người cho đến hết cuộc đời.

Dù người ta có dành tuổi trẻ của mình cho ai hay để làm gì, thì cũng mong sau này khi đã đủ chín chắn để ngoái đầu nhìn lại, họ cũng sẽ mỉm cười, một nụ cười vô ưu viên mãn.'
      ]);
      DB::table('SanPham')->insert([
        'loai' => 'kinhte',
        'ten' => 'Khoảng cách',
        'gia' => 69000,
        'linkanh' => 'khoangcach.jpg',
        'congtyphathanh' => 'Trí Việt',
        'nxb' => 'Nhà xuất bản Văn học',
        'tacgia' => 'Jack London',
        'dichgia' => 'Nhiều người dịch',
        'loaibia' => 'Bìa mềm',
        'sotrang' => 312,
        'ngayxuatban' => '10-2017',
        'sku' => '2519727431083',
        'noidungchinh' => 'Thanh Xuân Không Hối Tiếc

Mỗi người có một cách khác nhau để sống những ngày tuổi trẻ, có người dành trọn nó cho những cuộc tình, có người dành trọn nó cho công việc, có người dành trọn nó để tự yêu thương mình, và cũng có những người chia tuổi trẻ của mình ra, để yêu một vài người, sau đó yêu mình, yêu người xung quanh mình, rồi đến một lúc nào đó thích hợp mới tiếp tục muốn yêu thêm một người cho đến hết cuộc đời.

Dù người ta có dành tuổi trẻ của mình cho ai hay để làm gì, thì cũng mong sau này khi đã đủ chín chắn để ngoái đầu nhìn lại, họ cũng sẽ mỉm cười, một nụ cười vô ưu viên mãn.'
      ]);
      DB::table('SanPham')->insert([
        'loai' => 'kinhte',
        'ten' => 'Qua Pixar là vô cực',
        'gia' => 69000,
        'linkanh' => 'quapixarlavocuc.jpg',
        'congtyphathanh' => 'Trí Việt',
        'nxb' => 'Nhà xuất bản Văn học',
        'tacgia' => 'Jack London',
        'dichgia' => 'Nhiều người dịch',
        'loaibia' => 'Bìa mềm',
        'sotrang' => 312,
        'ngayxuatban' => '10-2017',
        'sku' => '2519727431083',
        'noidungchinh' => 'Thanh Xuân Không Hối Tiếc

Mỗi người có một cách khác nhau để sống những ngày tuổi trẻ, có người dành trọn nó cho những cuộc tình, có người dành trọn nó cho công việc, có người dành trọn nó để tự yêu thương mình, và cũng có những người chia tuổi trẻ của mình ra, để yêu một vài người, sau đó yêu mình, yêu người xung quanh mình, rồi đến một lúc nào đó thích hợp mới tiếp tục muốn yêu thêm một người cho đến hết cuộc đời.

Dù người ta có dành tuổi trẻ của mình cho ai hay để làm gì, thì cũng mong sau này khi đã đủ chín chắn để ngoái đầu nhìn lại, họ cũng sẽ mỉm cười, một nụ cười vô ưu viên mãn.'
      ]);
      DB::table('SanPham')->insert([
        'loai' => 'kinhte',
        'ten' => 'Đọc vị bất kì ai',
        'gia' => 69000,
        'linkanh' => 'docvibatkiai.jpg',
        'congtyphathanh' => 'Trí Việt',
        'nxb' => 'Nhà xuất bản Văn học',
        'tacgia' => 'Jack London',
        'dichgia' => 'Nhiều người dịch',
        'loaibia' => 'Bìa mềm',
        'sotrang' => 312,
        'ngayxuatban' => '10-2017',
        'sku' => '2519727431083',
        'noidungchinh' => 'Thanh Xuân Không Hối Tiếc

Mỗi người có một cách khác nhau để sống những ngày tuổi trẻ, có người dành trọn nó cho những cuộc tình, có người dành trọn nó cho công việc, có người dành trọn nó để tự yêu thương mình, và cũng có những người chia tuổi trẻ của mình ra, để yêu một vài người, sau đó yêu mình, yêu người xung quanh mình, rồi đến một lúc nào đó thích hợp mới tiếp tục muốn yêu thêm một người cho đến hết cuộc đời.

Dù người ta có dành tuổi trẻ của mình cho ai hay để làm gì, thì cũng mong sau này khi đã đủ chín chắn để ngoái đầu nhìn lại, họ cũng sẽ mỉm cười, một nụ cười vô ưu viên mãn.'
      ]);
      DB::table('SanPham')->insert([
        'loai' => 'kinhte',
        'ten' => 'Chiến thắng con quỷ trong bạn',
        'gia' => 69000,
        'linkanh' => 'chienthangconquytrongban.jpg',
        'congtyphathanh' => 'Trí Việt',
        'nxb' => 'Nhà xuất bản Văn học',
        'tacgia' => 'Jack London',
        'dichgia' => 'Nhiều người dịch',
        'loaibia' => 'Bìa mềm',
        'sotrang' => 312,
        'ngayxuatban' => '10-2017',
        'sku' => '2519727431083',
        'noidungchinh' => 'Thanh Xuân Không Hối Tiếc

Mỗi người có một cách khác nhau để sống những ngày tuổi trẻ, có người dành trọn nó cho những cuộc tình, có người dành trọn nó cho công việc, có người dành trọn nó để tự yêu thương mình, và cũng có những người chia tuổi trẻ của mình ra, để yêu một vài người, sau đó yêu mình, yêu người xung quanh mình, rồi đến một lúc nào đó thích hợp mới tiếp tục muốn yêu thêm một người cho đến hết cuộc đời.

Dù người ta có dành tuổi trẻ của mình cho ai hay để làm gì, thì cũng mong sau này khi đã đủ chín chắn để ngoái đầu nhìn lại, họ cũng sẽ mỉm cười, một nụ cười vô ưu viên mãn.'
      ]);

      DB::table('SanPham')->insert([
        'loai' => 'giaotrinh',
        'ten' => 'Phân tích chứng khoán',
        'gia' => 69000,
        'linkanh' => 'phantichchungkhoan.jpg',
        'congtyphathanh' => 'Trí Việt',
        'nxb' => 'Nhà xuất bản Văn học',
        'tacgia' => 'Jack London',
        'dichgia' => 'Nhiều người dịch',
        'loaibia' => 'Bìa mềm',
        'sotrang' => 312,
        'ngayxuatban' => '10-2017',
        'sku' => '2519727431083',
        'noidungchinh' => 'Thanh Xuân Không Hối Tiếc

Mỗi người có một cách khác nhau để sống những ngày tuổi trẻ, có người dành trọn nó cho những cuộc tình, có người dành trọn nó cho công việc, có người dành trọn nó để tự yêu thương mình, và cũng có những người chia tuổi trẻ của mình ra, để yêu một vài người, sau đó yêu mình, yêu người xung quanh mình, rồi đến một lúc nào đó thích hợp mới tiếp tục muốn yêu thêm một người cho đến hết cuộc đời.

Dù người ta có dành tuổi trẻ của mình cho ai hay để làm gì, thì cũng mong sau này khi đã đủ chín chắn để ngoái đầu nhìn lại, họ cũng sẽ mỉm cười, một nụ cười vô ưu viên mãn.'
      ]);
      DB::table('SanPham')->insert([
        'loai' => 'giaotrinh',
        'ten' => 'Tự học Excel',
        'gia' => 69000,
        'linkanh' => 'tuhocexcel.jpg',
        'congtyphathanh' => 'Trí Việt',
        'nxb' => 'Nhà xuất bản Văn học',
        'tacgia' => 'Jack London',
        'dichgia' => 'Nhiều người dịch',
        'loaibia' => 'Bìa mềm',
        'sotrang' => 312,
        'ngayxuatban' => '10-2017',
        'sku' => '2519727431083',
        'noidungchinh' => 'Thanh Xuân Không Hối Tiếc

Mỗi người có một cách khác nhau để sống những ngày tuổi trẻ, có người dành trọn nó cho những cuộc tình, có người dành trọn nó cho công việc, có người dành trọn nó để tự yêu thương mình, và cũng có những người chia tuổi trẻ của mình ra, để yêu một vài người, sau đó yêu mình, yêu người xung quanh mình, rồi đến một lúc nào đó thích hợp mới tiếp tục muốn yêu thêm một người cho đến hết cuộc đời.

Dù người ta có dành tuổi trẻ của mình cho ai hay để làm gì, thì cũng mong sau này khi đã đủ chín chắn để ngoái đầu nhìn lại, họ cũng sẽ mỉm cười, một nụ cười vô ưu viên mãn.'
      ]);
      DB::table('SanPham')->insert([
        'loai' => 'giaotrinh',
        'ten' => 'Sáng tạo thuật toán',
        'gia' => 69000,
        'linkanh' => 'sangtaothuattoan.jpg',
        'congtyphathanh' => 'Trí Việt',
        'nxb' => 'Nhà xuất bản Văn học',
        'tacgia' => 'Jack London',
        'dichgia' => 'Nhiều người dịch',
        'loaibia' => 'Bìa mềm',
        'sotrang' => 312,
        'ngayxuatban' => '10-2017',
        'sku' => '2519727431083',
        'noidungchinh' => 'Thanh Xuân Không Hối Tiếc

Mỗi người có một cách khác nhau để sống những ngày tuổi trẻ, có người dành trọn nó cho những cuộc tình, có người dành trọn nó cho công việc, có người dành trọn nó để tự yêu thương mình, và cũng có những người chia tuổi trẻ của mình ra, để yêu một vài người, sau đó yêu mình, yêu người xung quanh mình, rồi đến một lúc nào đó thích hợp mới tiếp tục muốn yêu thêm một người cho đến hết cuộc đời.

Dù người ta có dành tuổi trẻ của mình cho ai hay để làm gì, thì cũng mong sau này khi đã đủ chín chắn để ngoái đầu nhìn lại, họ cũng sẽ mỉm cười, một nụ cười vô ưu viên mãn.'
      ]);
      DB::table('SanPham')->insert([
        'loai' => 'giaotrinh',
        'ten' => 'Nguyên lý kế toán',
        'gia' => 69000,
        'linkanh' => 'nguyenlyketoan.jpg',
        'congtyphathanh' => 'Trí Việt',
        'nxb' => 'Nhà xuất bản Văn học',
        'tacgia' => 'Jack London',
        'dichgia' => 'Nhiều người dịch',
        'loaibia' => 'Bìa mềm',
        'sotrang' => 312,
        'ngayxuatban' => '10-2017',
        'sku' => '2519727431083',
        'noidungchinh' => 'Thanh Xuân Không Hối Tiếc

Mỗi người có một cách khác nhau để sống những ngày tuổi trẻ, có người dành trọn nó cho những cuộc tình, có người dành trọn nó cho công việc, có người dành trọn nó để tự yêu thương mình, và cũng có những người chia tuổi trẻ của mình ra, để yêu một vài người, sau đó yêu mình, yêu người xung quanh mình, rồi đến một lúc nào đó thích hợp mới tiếp tục muốn yêu thêm một người cho đến hết cuộc đời.

Dù người ta có dành tuổi trẻ của mình cho ai hay để làm gì, thì cũng mong sau này khi đã đủ chín chắn để ngoái đầu nhìn lại, họ cũng sẽ mỉm cười, một nụ cười vô ưu viên mãn.'
      ]);
      DB::table('SanPham')->insert([
        'loai' => 'giaotrinh',
        'ten' => 'Quản trị chiến lược',
        'gia' => 69000,
        'linkanh' => 'quantrichienluoc.jpg',
        'congtyphathanh' => 'Trí Việt',
        'nxb' => 'Nhà xuất bản Văn học',
        'tacgia' => 'Jack London',
        'dichgia' => 'Nhiều người dịch',
        'loaibia' => 'Bìa mềm',
        'sotrang' => 312,
        'ngayxuatban' => '10-2017',
        'sku' => '2519727431083',
        'noidungchinh' => 'Thanh Xuân Không Hối Tiếc

Mỗi người có một cách khác nhau để sống những ngày tuổi trẻ, có người dành trọn nó cho những cuộc tình, có người dành trọn nó cho công việc, có người dành trọn nó để tự yêu thương mình, và cũng có những người chia tuổi trẻ của mình ra, để yêu một vài người, sau đó yêu mình, yêu người xung quanh mình, rồi đến một lúc nào đó thích hợp mới tiếp tục muốn yêu thêm một người cho đến hết cuộc đời.

Dù người ta có dành tuổi trẻ của mình cho ai hay để làm gì, thì cũng mong sau này khi đã đủ chín chắn để ngoái đầu nhìn lại, họ cũng sẽ mỉm cười, một nụ cười vô ưu viên mãn.'
      ]);
      DB::table('SanPham')->insert([
        'loai' => 'giaotrinh',
        'ten' => 'Kế toán tài chính',
        'gia' => 69000,
        'linkanh' => 'ketoantaichinh.jpg',
        'congtyphathanh' => 'Trí Việt',
        'nxb' => 'Nhà xuất bản Văn học',
        'tacgia' => 'Jack London',
        'dichgia' => 'Nhiều người dịch',
        'loaibia' => 'Bìa mềm',
        'sotrang' => 312,
        'ngayxuatban' => '10-2017',
        'sku' => '2519727431083',
        'noidungchinh' => 'Thanh Xuân Không Hối Tiếc

Mỗi người có một cách khác nhau để sống những ngày tuổi trẻ, có người dành trọn nó cho những cuộc tình, có người dành trọn nó cho công việc, có người dành trọn nó để tự yêu thương mình, và cũng có những người chia tuổi trẻ của mình ra, để yêu một vài người, sau đó yêu mình, yêu người xung quanh mình, rồi đến một lúc nào đó thích hợp mới tiếp tục muốn yêu thêm một người cho đến hết cuộc đời.

Dù người ta có dành tuổi trẻ của mình cho ai hay để làm gì, thì cũng mong sau này khi đã đủ chín chắn để ngoái đầu nhìn lại, họ cũng sẽ mỉm cười, một nụ cười vô ưu viên mãn.'
      ]);

      DB::table('SanPham')->insert([
        'loai' => 'lichsu',
        'ten' => 'Sử Việt - 12 khúc tráng ca',
        'gia' => 69000,
        'linkanh' => 'suviet.jpg',
        'congtyphathanh' => 'Trí Việt',
        'nxb' => 'Nhà xuất bản Văn học',
        'tacgia' => 'Jack London',
        'dichgia' => 'Nhiều người dịch',
        'loaibia' => 'Bìa mềm',
        'sotrang' => 312,
        'ngayxuatban' => '10-2017',
        'sku' => '2519727431083',
        'noidungchinh' => 'Thanh Xuân Không Hối Tiếc

Mỗi người có một cách khác nhau để sống những ngày tuổi trẻ, có người dành trọn nó cho những cuộc tình, có người dành trọn nó cho công việc, có người dành trọn nó để tự yêu thương mình, và cũng có những người chia tuổi trẻ của mình ra, để yêu một vài người, sau đó yêu mình, yêu người xung quanh mình, rồi đến một lúc nào đó thích hợp mới tiếp tục muốn yêu thêm một người cho đến hết cuộc đời.

Dù người ta có dành tuổi trẻ của mình cho ai hay để làm gì, thì cũng mong sau này khi đã đủ chín chắn để ngoái đầu nhìn lại, họ cũng sẽ mỉm cười, một nụ cười vô ưu viên mãn.'
      ]);
      DB::table('SanPham')->insert([
        'loai' => 'lichsu',
        'ten' => 'Thế giới phẳng',
        'gia' => 69000,
        'linkanh' => 'thegioiphang.jpg',
        'congtyphathanh' => 'Trí Việt',
        'nxb' => 'Nhà xuất bản Văn học',
        'tacgia' => 'Jack London',
        'dichgia' => 'Nhiều người dịch',
        'loaibia' => 'Bìa mềm',
        'sotrang' => 312,
        'ngayxuatban' => '10-2017',
        'sku' => '2519727431083',
        'noidungchinh' => 'Thanh Xuân Không Hối Tiếc

Mỗi người có một cách khác nhau để sống những ngày tuổi trẻ, có người dành trọn nó cho những cuộc tình, có người dành trọn nó cho công việc, có người dành trọn nó để tự yêu thương mình, và cũng có những người chia tuổi trẻ của mình ra, để yêu một vài người, sau đó yêu mình, yêu người xung quanh mình, rồi đến một lúc nào đó thích hợp mới tiếp tục muốn yêu thêm một người cho đến hết cuộc đời.

Dù người ta có dành tuổi trẻ của mình cho ai hay để làm gì, thì cũng mong sau này khi đã đủ chín chắn để ngoái đầu nhìn lại, họ cũng sẽ mỉm cười, một nụ cười vô ưu viên mãn.'
      ]);
      DB::table('SanPham')->insert([
        'loai' => 'lichsu',
        'ten' => 'Lịch sử Việt Nam',
        'gia' => 69000,
        'linkanh' => 'lichsuvietnam.jpg',
        'congtyphathanh' => 'Trí Việt',
        'nxb' => 'Nhà xuất bản Văn học',
        'tacgia' => 'Jack London',
        'dichgia' => 'Nhiều người dịch',
        'loaibia' => 'Bìa mềm',
        'sotrang' => 312,
        'ngayxuatban' => '10-2017',
        'sku' => '2519727431083',
        'noidungchinh' => 'Thanh Xuân Không Hối Tiếc

Mỗi người có một cách khác nhau để sống những ngày tuổi trẻ, có người dành trọn nó cho những cuộc tình, có người dành trọn nó cho công việc, có người dành trọn nó để tự yêu thương mình, và cũng có những người chia tuổi trẻ của mình ra, để yêu một vài người, sau đó yêu mình, yêu người xung quanh mình, rồi đến một lúc nào đó thích hợp mới tiếp tục muốn yêu thêm một người cho đến hết cuộc đời.

Dù người ta có dành tuổi trẻ của mình cho ai hay để làm gì, thì cũng mong sau này khi đã đủ chín chắn để ngoái đầu nhìn lại, họ cũng sẽ mỉm cười, một nụ cười vô ưu viên mãn.'
      ]);
      DB::table('SanPham')->insert([
        'loai' => 'lichsu',
        'ten' => 'Napoleon Đại đế',
        'gia' => 69000,
        'linkanh' => 'napoleondaide.jpg',
        'congtyphathanh' => 'Trí Việt',
        'nxb' => 'Nhà xuất bản Văn học',
        'tacgia' => 'Jack London',
        'dichgia' => 'Nhiều người dịch',
        'loaibia' => 'Bìa mềm',
        'sotrang' => 312,
        'ngayxuatban' => '10-2017',
        'sku' => '2519727431083',
        'noidungchinh' => 'Thanh Xuân Không Hối Tiếc

Mỗi người có một cách khác nhau để sống những ngày tuổi trẻ, có người dành trọn nó cho những cuộc tình, có người dành trọn nó cho công việc, có người dành trọn nó để tự yêu thương mình, và cũng có những người chia tuổi trẻ của mình ra, để yêu một vài người, sau đó yêu mình, yêu người xung quanh mình, rồi đến một lúc nào đó thích hợp mới tiếp tục muốn yêu thêm một người cho đến hết cuộc đời.

Dù người ta có dành tuổi trẻ của mình cho ai hay để làm gì, thì cũng mong sau này khi đã đủ chín chắn để ngoái đầu nhìn lại, họ cũng sẽ mỉm cười, một nụ cười vô ưu viên mãn.'
      ]);
      DB::table('SanPham')->insert([
        'loai' => 'lichsu',
        'ten' => 'Câu chuyện Do Thái',
        'gia' => 69000,
        'linkanh' => 'cauchuyendothai.jpg',
        'congtyphathanh' => 'Trí Việt',
        'nxb' => 'Nhà xuất bản Văn học',
        'tacgia' => 'Jack London',
        'dichgia' => 'Nhiều người dịch',
        'loaibia' => 'Bìa mềm',
        'sotrang' => 312,
        'ngayxuatban' => '10-2017',
        'sku' => '2519727431083',
        'noidungchinh' => 'Thanh Xuân Không Hối Tiếc

Mỗi người có một cách khác nhau để sống những ngày tuổi trẻ, có người dành trọn nó cho những cuộc tình, có người dành trọn nó cho công việc, có người dành trọn nó để tự yêu thương mình, và cũng có những người chia tuổi trẻ của mình ra, để yêu một vài người, sau đó yêu mình, yêu người xung quanh mình, rồi đến một lúc nào đó thích hợp mới tiếp tục muốn yêu thêm một người cho đến hết cuộc đời.

Dù người ta có dành tuổi trẻ của mình cho ai hay để làm gì, thì cũng mong sau này khi đã đủ chín chắn để ngoái đầu nhìn lại, họ cũng sẽ mỉm cười, một nụ cười vô ưu viên mãn.'
      ]);
      DB::table('SanPham')->insert([
        'loai' => 'lichsu',
        'ten' => 'Đại Việt sử kí toàn thư',
        'gia' => 69000,
        'linkanh' => 'daivietsukitoanthu.jpg',
        'congtyphathanh' => 'Trí Việt',
        'nxb' => 'Nhà xuất bản Văn học',
        'tacgia' => 'Jack London',
        'dichgia' => 'Nhiều người dịch',
        'loaibia' => 'Bìa mềm',
        'sotrang' => 312,
        'ngayxuatban' => '10-2017',
        'sku' => '2519727431083',
        'noidungchinh' => 'Thanh Xuân Không Hối Tiếc

Mỗi người có một cách khác nhau để sống những ngày tuổi trẻ, có người dành trọn nó cho những cuộc tình, có người dành trọn nó cho công việc, có người dành trọn nó để tự yêu thương mình, và cũng có những người chia tuổi trẻ của mình ra, để yêu một vài người, sau đó yêu mình, yêu người xung quanh mình, rồi đến một lúc nào đó thích hợp mới tiếp tục muốn yêu thêm một người cho đến hết cuộc đời.

Dù người ta có dành tuổi trẻ của mình cho ai hay để làm gì, thì cũng mong sau này khi đã đủ chín chắn để ngoái đầu nhìn lại, họ cũng sẽ mỉm cười, một nụ cười vô ưu viên mãn.'
      ]);

      DB::table('SanPham')->insert([
        'loai' => 'khkt',
        'ten' => 'Nhân tố Enzyme',
        'gia' => 69000,
        'linkanh' => 'nhanhtoenzym.jpg',
        'congtyphathanh' => 'Trí Việt',
        'nxb' => 'Nhà xuất bản Văn học',
        'tacgia' => 'Jack London',
        'dichgia' => 'Nhiều người dịch',
        'loaibia' => 'Bìa mềm',
        'sotrang' => 312,
        'ngayxuatban' => '10-2017',
        'sku' => '2519727431083',
        'noidungchinh' => 'Thanh Xuân Không Hối Tiếc

Mỗi người có một cách khác nhau để sống những ngày tuổi trẻ, có người dành trọn nó cho những cuộc tình, có người dành trọn nó cho công việc, có người dành trọn nó để tự yêu thương mình, và cũng có những người chia tuổi trẻ của mình ra, để yêu một vài người, sau đó yêu mình, yêu người xung quanh mình, rồi đến một lúc nào đó thích hợp mới tiếp tục muốn yêu thêm một người cho đến hết cuộc đời.

Dù người ta có dành tuổi trẻ của mình cho ai hay để làm gì, thì cũng mong sau này khi đã đủ chín chắn để ngoái đầu nhìn lại, họ cũng sẽ mỉm cười, một nụ cười vô ưu viên mãn.'
      ]);
      DB::table('SanPham')->insert([
        'loai' => 'khkt',
        'ten' => 'Minh Triết trong ăn uống của phương Đông',
        'gia' => 69000,
        'linkanh' => 'mnhtriet.jpg',
        'congtyphathanh' => 'Trí Việt',
        'nxb' => 'Nhà xuất bản Văn học',
        'tacgia' => 'Jack London',
        'dichgia' => 'Nhiều người dịch',
        'loaibia' => 'Bìa mềm',
        'sotrang' => 312,
        'ngayxuatban' => '10-2017',
        'sku' => '2519727431083',
        'noidungchinh' => 'Thanh Xuân Không Hối Tiếc

Mỗi người có một cách khác nhau để sống những ngày tuổi trẻ, có người dành trọn nó cho những cuộc tình, có người dành trọn nó cho công việc, có người dành trọn nó để tự yêu thương mình, và cũng có những người chia tuổi trẻ của mình ra, để yêu một vài người, sau đó yêu mình, yêu người xung quanh mình, rồi đến một lúc nào đó thích hợp mới tiếp tục muốn yêu thêm một người cho đến hết cuộc đời.

Dù người ta có dành tuổi trẻ của mình cho ai hay để làm gì, thì cũng mong sau này khi đã đủ chín chắn để ngoái đầu nhìn lại, họ cũng sẽ mỉm cười, một nụ cười vô ưu viên mãn.'
      ]);
      DB::table('SanPham')->insert([
        'loai' => 'khkt',
        'ten' => 'Các thế giới song song',
        'gia' => 69000,
        'linkanh' => 'cacthegioisongsong.jpg',
        'congtyphathanh' => 'Trí Việt',
        'nxb' => 'Nhà xuất bản Văn học',
        'tacgia' => 'Jack London',
        'dichgia' => 'Nhiều người dịch',
        'loaibia' => 'Bìa mềm',
        'sotrang' => 312,
        'ngayxuatban' => '10-2017',
        'sku' => '2519727431083',
        'noidungchinh' => 'Thanh Xuân Không Hối Tiếc

Mỗi người có một cách khác nhau để sống những ngày tuổi trẻ, có người dành trọn nó cho những cuộc tình, có người dành trọn nó cho công việc, có người dành trọn nó để tự yêu thương mình, và cũng có những người chia tuổi trẻ của mình ra, để yêu một vài người, sau đó yêu mình, yêu người xung quanh mình, rồi đến một lúc nào đó thích hợp mới tiếp tục muốn yêu thêm một người cho đến hết cuộc đời.

Dù người ta có dành tuổi trẻ của mình cho ai hay để làm gì, thì cũng mong sau này khi đã đủ chín chắn để ngoái đầu nhìn lại, họ cũng sẽ mỉm cười, một nụ cười vô ưu viên mãn.'
      ]);
      DB::table('SanPham')->insert([
        'loai' => 'khkt',
        'ten' => 'Phù thủy số học',
        'gia' => 69000,
        'linkanh' => 'phuthuysohoc.jpg',
        'congtyphathanh' => 'Trí Việt',
        'nxb' => 'Nhà xuất bản Văn học',
        'tacgia' => 'Jack London',
        'dichgia' => 'Nhiều người dịch',
        'loaibia' => 'Bìa mềm',
        'sotrang' => 312,
        'ngayxuatban' => '10-2017',
        'sku' => '2519727431083',
        'noidungchinh' => 'Thanh Xuân Không Hối Tiếc

Mỗi người có một cách khác nhau để sống những ngày tuổi trẻ, có người dành trọn nó cho những cuộc tình, có người dành trọn nó cho công việc, có người dành trọn nó để tự yêu thương mình, và cũng có những người chia tuổi trẻ của mình ra, để yêu một vài người, sau đó yêu mình, yêu người xung quanh mình, rồi đến một lúc nào đó thích hợp mới tiếp tục muốn yêu thêm một người cho đến hết cuộc đời.

Dù người ta có dành tuổi trẻ của mình cho ai hay để làm gì, thì cũng mong sau này khi đã đủ chín chắn để ngoái đầu nhìn lại, họ cũng sẽ mỉm cười, một nụ cười vô ưu viên mãn.'
      ]);
      DB::table('SanPham')->insert([
        'loai' => 'khkt',
        'ten' => 'Code dạo ký sự',
        'gia' => 69000,
        'linkanh' => 'codedaokysu.jpg',
        'congtyphathanh' => 'Trí Việt',
        'nxb' => 'Nhà xuất bản Văn học',
        'tacgia' => 'Jack London',
        'dichgia' => 'Nhiều người dịch',
        'loaibia' => 'Bìa mềm',
        'sotrang' => 312,
        'ngayxuatban' => '10-2017',
        'sku' => '2519727431083',
        'noidungchinh' => 'Thanh Xuân Không Hối Tiếc

Mỗi người có một cách khác nhau để sống những ngày tuổi trẻ, có người dành trọn nó cho những cuộc tình, có người dành trọn nó cho công việc, có người dành trọn nó để tự yêu thương mình, và cũng có những người chia tuổi trẻ của mình ra, để yêu một vài người, sau đó yêu mình, yêu người xung quanh mình, rồi đến một lúc nào đó thích hợp mới tiếp tục muốn yêu thêm một người cho đến hết cuộc đời.

Dù người ta có dành tuổi trẻ của mình cho ai hay để làm gì, thì cũng mong sau này khi đã đủ chín chắn để ngoái đầu nhìn lại, họ cũng sẽ mỉm cười, một nụ cười vô ưu viên mãn.'
      ]);
      DB::table('SanPham')->insert([
        'loai' => 'khkt',
        'ten' => 'Phòng và chữa bệnh ung thư theo phương thức tự nhiên',
        'gia' => 69000,
        'linkanh' => 'phongchuaungthu.jpg',
        'congtyphathanh' => 'Trí Việt',
        'nxb' => 'Nhà xuất bản Văn học',
        'tacgia' => 'Jack London',
        'dichgia' => 'Nhiều người dịch',
        'loaibia' => 'Bìa mềm',
        'sotrang' => 312,
        'ngayxuatban' => '10-2017',
        'sku' => '2519727431083',
        'noidungchinh' => 'Thanh Xuân Không Hối Tiếc

Mỗi người có một cách khác nhau để sống những ngày tuổi trẻ, có người dành trọn nó cho những cuộc tình, có người dành trọn nó cho công việc, có người dành trọn nó để tự yêu thương mình, và cũng có những người chia tuổi trẻ của mình ra, để yêu một vài người, sau đó yêu mình, yêu người xung quanh mình, rồi đến một lúc nào đó thích hợp mới tiếp tục muốn yêu thêm một người cho đến hết cuộc đời.

Dù người ta có dành tuổi trẻ của mình cho ai hay để làm gì, thì cũng mong sau này khi đã đủ chín chắn để ngoái đầu nhìn lại, họ cũng sẽ mỉm cười, một nụ cười vô ưu viên mãn.'
      ]);
    }
}
