<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnSanpham extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('SanPham', function (Blueprint $table) {
        $table->string('congtyphathanh');
        $table->string('nxb');
        $table->string('tacgia');
        $table->string('dichgia')->nullable();
        $table->string('loaibia');
        $table->integer('sotrang');
        $table->string('ngayxuatban');
        $table->string('sku');
        $table->text('noidungchinh');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('SanPham', function (Blueprint $table) {
        $table->dropColumn(['congtyphathanh', 'nxb', 'tacgia', 'dichgia', 'loaibia', 'sotrang', 'ngayxuatban', 'sku']);
      });
    }
}
