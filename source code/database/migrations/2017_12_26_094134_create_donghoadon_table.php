<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDonghoadonTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('donghoadon', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('hd_id')->unsigned();
            $table->integer('sp_id')->unsigned();
            $table->integer('giamua')->unsigned();
            $table->integer('giaban')->unsigned();
            $table->integer('soluong')->unsigned();
            $table->foreign('hd_id')->references('id')->on('hoadon');
            $table->foreign('sp_id')->references('id')->on('SanPham');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('donghoadon');
    }
}
