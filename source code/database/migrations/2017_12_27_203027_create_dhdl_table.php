<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDhdlTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('donghoadonluong', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('nv_id')->unsigned();
            $table->integer('hdl_id')->unsigned();
            $table->integer('sotien');
            $table->foreign('nv_id')->references('id')->on('nhanvien');
            $table->foreign('hdl_id')->references('id')->on('hoadonluong');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('donghoadonluong');
    }
}
