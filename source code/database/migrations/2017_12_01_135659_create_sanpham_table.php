<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSanphamTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('SanPham', function ($table) {
              $table->increments('id');
              $table->string('loai'); //loai sach
              $table->string('ten');
              $table->integer('gia');
              $table->integer('conlai')->default(100);  //so luong san pham con lai
              $table->integer('daban')->default(0); //so luong san pham da ban
              $table->integer('likes')->default(0); //so luong luot thich
              $table->string('linkanh'); //link chua anh cua san pham
              $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('SanPham');
    }
}
